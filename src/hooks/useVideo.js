import { useCallback, useState } from 'react';
import { useApi } from '../service/axios';

/**
 * @return {[{pending: boolean, errorMessage: string, data: {}}, {getVideo: ((function(): void)|*)}]}
 */
const useVideo = () => {
  const [pending, setPending] = useState(false);
  const [data, setData] = useState({});
  const [errorMessage, setErrorMessage] = useState('');
  const axios = useApi();
  const getVideo = useCallback(() => {
    setPending(true);
    //todo pending
    axios
      .get('api/public/stream?format=hls&metadata=full&video_id=16453')
      .then((result) => {
        setData(result.data);
      })
      .catch((error) => {
        setErrorMessage(error?.response?.data?.message);
      })
      .finally(() => {
        setPending(false);
      });
  }, [axios]);
  return [{ data, errorMessage, pending }, { getVideo }];
};

export default useVideo;
