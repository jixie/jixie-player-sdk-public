import { useCallback, useState } from 'react';
import { useApi } from '../service/axios';
const useTrack = () => {
  const [pending, setPending] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [result, setResult] = useState();
  const slackApi =
    'https://hooks.slack.com/services/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';
  const slackHeader = {
    'Content-type': 'application/json',
  };
  const axios = useApi();
  const tracker = useCallback(
    (params, aggregateTracking) => {
      const stringParams = {
        blocks: [
          {
            type: 'divider',
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `*${params.action}* 
              videoid: ${params.videoid}
              elapsedms :${params.elapsedms}
              vposition: ${params.vposition}
              autoplay: ${params.autoplay} 
              startmode: ${params.startmode} 
              volume: ${params.volume}
              playhead: ${params.playhead} 
              rendition: ${params.rendition}
              viewability: ${params.viewability}
              origtech: ${params.origtech}
              realtech: ${params.realtech}
              accountid: ${params.accountid}
              ownerid: ${params.ownerid}
              device: ${params.device}
              p_domain: ${params.p_domain}
              domain: ${params.domain}
              pageurl: ${params.pageurl}
              browser: ${params.browser} 
              os: ${params.os}
              client_id: ${params.client_id}
              session_id: ${params.session_id} 
              errormessage: ${params.errormessage || ''}
              errorcode: ${params.errorcode || ''}
              adTagUrl: ${params.adTagUrl || ''}
              ${params.step ? 'step: 5' : ''}
              `,
            },
          },
        ],
      };
      const aggregateParams = {
        blocks: [
          {
            type: 'divider',
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `action: duragg
              accountid: ${params.accountid}
              ownerid: ${params.ownerid}
              rendition: ${params.rendition}
              videoid: ${params.videoid}
              step: ${params.step}`,
            },
          },
        ],
      };
      setPending(true);
      axios
        .get('https://traid.jixie.io/sync/video', { params })
        .then((result) => {
          axios
            .post(
              slackApi,
              aggregateTracking ? aggregateParams : stringParams,
              { headers: slackHeader }
            )
            .finally((errorMessage) => errorMessage);
          setResult(result.data);
        })
        .catch((error) => {
          setErrorMessage(error?.response?.data?.message);
        })
        .finally(() => {
          setPending(false);
        });
    },
    [axios]
  );
  return [{ result, errorMessage, pending }, { tracker }];
};

export default useTrack;
