import React, {
  useEffect,
  useImperativeHandle,
  useReducer,
  useRef,
} from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import Video from 'react-native-video-inc-ads';
import { Provider as AxiosProvider } from './service/axios';
import axios from 'axios';
import { config } from './config';
import { SDKContext } from './context/SDKContext';
import { reducer } from './context/SDKReducer';
import Wrapper from './component/Wrapper';
import initialState from './initialState';

let ViewPropTypesVar;

if (ViewPropTypes) {
  ViewPropTypesVar = ViewPropTypes;
} else {
  ViewPropTypesVar = View.propTypes;
}

/**
 * @param props
 * @param ref
 * @return {JSX.Element}
 * @constructor
 */
const VideoPlayer = React.forwardRef((props, ref) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const wrapperRef = useRef(null);

  /**
   * set videoId or videoUrl to context store
   */
  useEffect(() => {
    props.videos && dispatch({ type: 'SET_VIDEO_RAW', videos: props.videos });
  }, [props.videos]);

  /**
   * set mainAdUnit to context store
   */
  useEffect(() => {
    if (
      state.mainAdUnit.id !== props.mainAdUnit.id ||
      state.mainAdUnit.delay !== props.mainAdUnit.delay
    ) {
      dispatch({ type: 'SET_MAIN_AD_UNIT', mainAdUnit: props.mainAdUnit });
    }
  }, [props.mainAdUnit]);

  useImperativeHandle(ref, () => ({
    load: (videos) => {
      dispatch({ type: 'SET_VIDEO_RAW', videos: videos });
    },
    play: () => {
      onPlay();
    },
    nextVideo: () => {
      wrapperRef.current.nextVideo();
    },
    pause: () => {
      dispatch({ type: 'PAUSE' });
    },
    metadata: () => {
      return state.metadata;
    },
  }));

  /**
   *set layout of device
   */
  function onLayout(event) {
    const { width } = event.nativeEvent.layout;
    if (width !== state.width) {
      dispatch({ type: 'SET_LAYOUT', width: width });
    }
  }

  let controlsTimeout = null;

  /**
   * set default values
   */
  useEffect(() => {
    if (props.autoplay) {
      hideControls(48);
    }
    if (controlsTimeout) {
      controlsTimeout = null;
      return clearTimeout(controlsTimeout);
    }
    props.accountId &&
      dispatch({ type: 'SET_ACCOUNT_ID', accountId: props.accountId });
    props.appName && dispatch({ type: 'SET_APP_NAME', appName: props.appName });
    props.pageName &&
      dispatch({ type: 'SET_PAGE_NAME', pageName: props.pageName });
    props.jixieAccessKey &&
      dispatch({ type: 'SET_ACCESS_KEY', accessKey: props.jixieAccessKey });
    props.clientId &&
      dispatch({ type: 'SET_CLIENT_ID', clientId: props.clientId });
  }, []);

  /**
   * hide the video controls bar
   */
  function hideControls(index) {
    if (props.onHideControls) {
      props.onHideControls();
    }

    if (controlsTimeout) {
      clearTimeout(controlsTimeout);
      controlsTimeout = null;
    }
    controlsTimeout = setTimeout(() => {
      dispatch({ type: 'HIDE_CONTROLS', index: index });
    }, 6000);
  }

  /**
   * display the video controls bar
   */
  const showControls = () => {
    if (props.onShowControls) {
      props.onShowControls();
    }
    dispatch({ type: 'SHOW_CONTROLS' });
    hideControls(87);
  };

  /**
   * handle video play action
   */
  function onPlay() {
    dispatch({
      type: 'ON_START',
      isPlaying: true,
      isStarted: true,
      hasEnded: false,
      progress: state.progress === 1 ? 0 : state.progress,
    });
  }

  /**
   * handle video start action
   */
  function onStartPress() {
    if (props.onStart) {
      props.onStart();
    }
    dispatch({
      type: 'ON_START',
      isPlaying: true,
      isStarted: true,
      hasEnded: false,
      progress: state.progress === 1 ? 0 : state.progress,
    });
    hideControls(101);
  }

  const onGetLink = (link) => {
    if (props.onGetLink) {
      props.onGetLink(link);
    }
  };

  /**
   * Wrap the SDK Player component inside SDK context and axios
   * -The SDK context to store state of the SDK
   * -The axios provides functions to call API
   */
  return (
    <AxiosProvider axios={axios.create(config.axios)}>
      <SDKContext.Provider value={{ state, dispatch }}>
        <View onLayout={onLayout}>
          <Wrapper
            onEnded={props.onEnded}
            ref={wrapperRef}
            onEvent={props.onEvent}
            onStartPress={onStartPress}
            showControls={showControls}
            showDuration={true}
            onGetLink={onGetLink}
            styles={props.styles}
            onGetAdTagUrl={props.onGetAdTagUrl}
            currentVideoIndex={props.currentVideoIndex}
            onVideoChange={props.onVideoChange}
            onPlaying={props.onPlaying}
          />
        </View>
      </SDKContext.Provider>
    </AxiosProvider>
  );
});

VideoPlayer.displayName = 'VideoPlayer';
/**
 *
 * @type {{onProgress: Requireable<(...args: any[]) => any>, hotspot: Requireable<InferProps<{maximum: Requireable<number>, id: Requireable<string>, hsDelay: Requireable<number>, hsDuration: Requireable<number>}>>, onGetAdTagUrl: Requireable<(...args: any[]) => any>, videos: Requireable<any[]>, onMutePress: Requireable<(...args: any[]) => any>, pageName: Requireable<string>, showDuration: Requireable<boolean>, videoHeight: Requireable<number>, videoWidth: Requireable<number>, duration: Requireable<number>, onEnded: Requireable<(...args: any[]) => any>, onStart: Requireable<(...args: any[]) => any>, onVideoChange: Requireable<(...args: any[]) => any>, onLoadVideoError: Requireable<(...args: any[]) => any>, id: Requireable<number>, resizeMode: Requireable<string>, clientId: Requireable<string>, jixieAccessKey: Requireable<string>, appName: Requireable<string>, controlsTimeout: Requireable<number>, onHideControls: Requireable<(...args: any[]) => any>, quality: Requireable<string>, mainAdUnit: Requireable<InferProps<{delay: Requireable<string>, id: Requireable<string>}>>, accountId: Requireable<string>, onPlaying: Requireable<(...args: any[]) => any>, onEvent: Requireable<(...args: any[]) => any>, onGetLink: Requireable<(...args: any[]) => any>, onLoad: Requireable<(...args: any[]) => any>, adMidRoll: Requireable<InferProps<{maximum: Requireable<number>, interval: Requireable<number>, id: Requireable<string>, minimum: Requireable<number>}>>, onPlayPress: Requireable<(...args: any[]) => any>, onShowControls: Requireable<(...args: any[]) => any>}}
 */
VideoPlayer.propTypes = {
  jixieAccessKey: PropTypes.string,
  clientId: PropTypes.string,
  onGetAdTagUrl: PropTypes.func,
  onEvent: PropTypes.func,
  onLoadVideoError: PropTypes.func,
  videos: PropTypes.array,
  accountId: PropTypes.string,
  quality: PropTypes.string,
  id: PropTypes.number,
  appName: PropTypes.string,
  pageName: PropTypes.string,
  onPlaying: PropTypes.func,
  mainAdUnit: PropTypes.shape({
    id: PropTypes.string,
    delay: PropTypes.string,
  }),
  adMidRoll: PropTypes.shape({
    id: PropTypes.string,
    interval: PropTypes.number,
    minimum: PropTypes.number,
    maximum: PropTypes.number,
  }),
  hotspot: PropTypes.shape({
    id: PropTypes.string,
    hsDelay: PropTypes.number,
    hsDuration: PropTypes.number,
    maximum: PropTypes.number,
  }),
  onGetLink: PropTypes.func,
  onLoad: PropTypes.func,
  onPlayPress: PropTypes.func,
  onHideControls: PropTypes.func,
  onShowControls: PropTypes.func,
  onMutePress: PropTypes.func,
  showDuration: PropTypes.bool,
  onEnded: PropTypes.func,
  onProgress: PropTypes.func,
  onStart: PropTypes.func,
  onVideoChange: PropTypes.func,
  videoWidth: PropTypes.number,
  videoHeight: PropTypes.number,
  duration: PropTypes.number,
  controlsTimeout: PropTypes.number,
  resizeMode: Video.propTypes.resizeMode,
};

VideoPlayer.defaultProps = {
  autoplay: false,
  controlsTimeout: 6000,
  loop: false,
  resizeMode: 'contain',
  fullScreenOnLongPress: false,
  customStyles: {},
  showDuration: false,
};

export default VideoPlayer;
