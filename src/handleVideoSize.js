import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export const handleVideoSize = (videoWidth, videoHeight, widthOverride) => {
  const _width = widthOverride ? widthOverride : width;
  const ratio = _width / videoWidth;

  return {
    height: videoHeight * ratio,
    width: _width,
  };
};
