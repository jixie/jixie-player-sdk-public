import React, { useRef, useState } from 'react';
import {
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { ControlsBar } from './ControlsBar';
import Video from 'react-native-video';
import { styles } from '../Styles';
import SeekBar from './SeekBar';
import { getDurationTime } from '../handleTime';
import { useSDK } from '../context/SDKContext';
import { handleVideoSize } from '../handleVideoSize';
import Icon from 'react-native-vector-icons/MaterialIcons';

const VideoComponent = (props) => {
  const { state, dispatch } = useSDK();
  const timeRef = useRef(null);
  const playerRef = useRef(null);
  const [seekTouchStart, setSeekTouchStart] = useState(0);
  const [seekProgressStart, setSeekProgressStart] = useState(0);
  const [seekBarWidth, setSeekBarWidth] = useState(200);
  const [wasPlayingBeforeSeek, setWasPlayingBeforeSeek] = useState(
    props.autoplay
  );

  const onSeekBarLayout = (nativeEvent) => {
    const customStyle = props.customStyles.seekBar;
    let padding;
    if (customStyle && customStyle.paddingHorizontal) {
      padding = customStyle.paddingHorizontal * 2;
    } else if (customStyle) {
      padding = customStyle.paddingLeft || 0;
      padding += customStyle.paddingRight ? customStyle.paddingRight : 0;
    } else {
      //circle size
      padding = 20;
    }
    setSeekBarWidth(nativeEvent.layout.width - padding);
  };

  const onSeekGrant = (e) => {
    setSeekTouchStart(e.nativeEvent.pageX);
    setSeekProgressStart(state.progress);
    setWasPlayingBeforeSeek(state.isPlaying);
    dispatch({ type: 'ON_SEEK_GRANT', isSeeking: true, isPlaying: false });
  };

  const onSeek = (e) => {
    const diff = e.nativeEvent.pageX - seekTouchStart;
    const ratio = 100 / seekBarWidth;
    const progress = seekProgressStart + (ratio * diff) / 100;
    dispatch({ type: 'ON_SEEK', progress: progress });
    playerRef.current.seek(progress * state.duration);
  };

  const onSeekRelease = () => {
    dispatch({
      type: 'ON_SEEK_RELEASE',
      isSeeking: false,
      isPlaying: wasPlayingBeforeSeek,
    });
    props.showControls();
  };

  function onProgress(event) {
    if (state.isSeeking) {
      return;
    }
    if (props.onProgress) {
      props.onProgress(event);
    }
    dispatch({
      type: 'ON_PROGRESS',
      progress: event.currentTime / (props.duration || state.duration),
    });
    timeRef.current?.setNativeProps({
      text: getDurationTime(event.currentTime),
    });
  }

  const onEnd = (event) => {
    if (props.onEnd) {
      props.onEnd(event);
    }
    dispatch({ type: 'ON_END', progress: 1 });

    if (!props.loop) {
      dispatch({ type: 'ON_LOOP', isPlaying: false });
      playerRef && playerRef.current.seek(0);
    } else {
      playerRef.current.seek(0);
    }

    timeRef.current?.setNativeProps({
      text: getDurationTime(state.duration),
    });
  };

  const onLoad = (event) => {
    if (props.onLoad) {
      props.onLoad(event);
    }
    const { duration } = event;
    dispatch({ type: 'ON_LOAD', duration: duration });
  };

  const onPlayPress = () => {
    if (props.onPlayPress) {
      props.onPlayPress();
    }
    dispatch({ type: 'ON_PLAY', isPlaying: !state.isPlaying });
    props.showControls();
  };

  const onMutePress = () => {
    const isMuted = !state.isMuted;
    if (props.onMutePress) {
      props.onMutePress(isMuted);
    }
    dispatch({ type: 'ON_MUTE', isMuted: isMuted });
    props.showControls();
  };

  const onToggleFullScreen = () => {
    playerRef.current.presentFullscreenPlayer();
  };

  const onSeekEvent = (e) => {
    timeRef.current?.setNativeProps({ text: getDurationTime(e.currentTime) });
  };

  const {
    video,
    style,
    resizeMode,
    pauseOnPress,
    fullScreenOnLongPress,
    customStyles,
  } = props;

  const renderControls = () => {
    return (
      <View style={[styles.controls, customStyles.controls]}>
        <TouchableOpacity
          onPress={onPlayPress}
          style={[customStyles.controlButton, customStyles.playControl]}
        >
          <Icon
            style={[
              styles.playControl,
              customStyles.controlIcon,
              customStyles.playIcon,
            ]}
            name={state.isPlaying ? 'pause' : 'play-arrow'}
            size={32}
          />
        </TouchableOpacity>
        <SeekBar
          disableSeek={props.disableSeek}
          customStyles={customStyles}
          fullWidth={false}
          onSeek={onSeek}
          onSeekBarLayout={onSeekBarLayout}
          onSeekGrant={onSeekGrant}
          onSeekRelease={onSeekRelease}
        />
        {props.showDuration && (
          <>
            <TextInput
              style={[
                styles.durationText,
                styles.activeDurationText,
                customStyles.durationText,
              ]}
              editable={false}
              ref={timeRef}
              value={getDurationTime(0)}
            />
            <Text style={[styles.durationText, customStyles.durationText]}>
              /
            </Text>
            <Text style={[styles.durationText, customStyles.durationText]}>
              {getDurationTime(state.duration)}
            </Text>
          </>
        )}
        {props.muted ? null : (
          <TouchableOpacity
            onPress={onMutePress}
            style={customStyles.controlButton}
          >
            <Icon
              style={[styles.extraControl]}
              name={state.isMuted ? 'volume-off' : 'volume-up'}
              size={24}
            />
          </TouchableOpacity>
        )}
        {Platform.OS === 'android' || props.disableFullscreen ? null : (
          <TouchableOpacity
            onPress={onToggleFullScreen}
            style={customStyles.controlButton}
          >
            <Icon style={[styles.extraControl]} name="fullscreen" size={32} />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  return (
    <View style={customStyles.videoWrapper}>
      <ControlsBar />
      <Video
        style={[
          styles.video,
          handleVideoSize(
            state?.currentVideoInfo?.videoQuality.width,
            state?.currentVideoInfo?.videoQuality.height,
            state.width
          ),
          style,
          customStyles.video,
        ]}
        ref={playerRef}
        muted={props.muted || state.isMuted}
        paused={
          props.paused ? props.paused || !state.isPlaying : !state.isPlaying
        }
        onProgress={onProgress}
        onEnd={onEnd}
        onLoad={onLoad}
        source={video}
        resizeMode={'contain'}
        onSeek={onSeekEvent}
      />
      <View
        style={[
          handleVideoSize(
            state?.currentVideoInfo?.videoQuality.width,
            state?.currentVideoInfo?.videoQuality.height,
            state.width
          ),
          {
            marginTop: -handleVideoSize(
              state?.currentVideoInfo?.videoQuality.width,
              state?.currentVideoInfo?.videoQuality.height,
              state.width
            ).height,
          },
        ]}
      >
        <TouchableOpacity
          style={styles.overlayButton}
          onPress={() => {
            props.showControls();
            if (pauseOnPress) onPlayPress();
          }}
          onLongPress={() => {
            if (fullScreenOnLongPress && Platform.OS !== 'android')
              onToggleFullScreen();
          }}
        />
      </View>
      {!state.isPlaying || state.isControlsVisible ? (
        renderControls()
      ) : (
        <SeekBar
          fullWidth={true}
          customStyles={customStyles}
          disableSeek={props.disableSeek}
          onSeek={onSeek}
          onSeekBarLayout={onSeekBarLayout}
          onSeekGrant={onSeekGrant}
          onSeekRelease={onSeekRelease}
        />
      )}
    </View>
  );
};

export default VideoComponent;
