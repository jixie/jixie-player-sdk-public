import React from 'react';
import { TouchableOpacity } from 'react-native';
import { styles } from '../Styles';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const StartButton = ({ onStartPress }) => {
  return (
    <TouchableOpacity
      style={[styles.playButton]}
      onPress={() => onStartPress()}
    >
      <Icon style={[styles.playArrow]} name="play-arrow" size={42} />
    </TouchableOpacity>
  );
};

export default StartButton;
