import Icon from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity, View, Text, ScrollView } from 'react-native';
import ActionSheet from 'react-native-actions-sheet';
import React, { createRef, useEffect } from 'react';
import useVideo from '../hooks/useVideo';
import { styles } from '../Styles';

const actionSheetRef = createRef();

//dependence Api
const VideoQuality = [
  {
    quality: '1080p',
    value: 1080,
    used: false,
  },
  {
    quality: '720p',
    value: 720,
    used: true,
  },
  {
    quality: '480p',
    value: 480,
    used: false,
  },
  {
    quality: '360p',
    value: 360,
    used: false,
  },
  {
    quality: '240p',
    value: 240,
    used: false,
  },
];
export const ControlsBar = () => {
  const [{ video, errorMessage, pending }, { getVideo }] = useVideo();
  useEffect(() => {
    getVideo();
  }, []);
  // console.log(video);
  return (
    <View style={styles.controlContainer}>
      <TouchableOpacity
        onPress={() => {
          console.log('more');
          actionSheetRef.current?.setModalVisible();
        }}
      >
        <Icon name={'more-vert'} size={20} />
      </TouchableOpacity>

      <ActionSheet ref={actionSheetRef}>
        <ScrollView style={styles.controlScroll}>
          <Text style={styles.qualityControlTitle}>Video quality</Text>
          <View style={styles.backgroundControl}>
            {VideoQuality.map((element) => {
              return (
                <View
                  key={`index${element.quality}`}
                  style={{
                    ...styles.qualityDetail,
                    backgroundColor: element.used ? '#212121' : '#E5E5E5',
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      color: element.used ? '#FFF' : '#212121',
                    }}
                  >
                    {element.quality}
                  </Text>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </ActionSheet>
    </View>
  );
};
