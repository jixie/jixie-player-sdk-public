import React from 'react';
import { styles } from '../Styles';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useSDK } from '../context/SDKContext';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const SelectView = () => {
  const { state, dispatch } = useSDK();
  const videoQualities = state?.currentVideoInfo?.videoQualities || [];
  const videoQuality = state?.currentVideoInfo?.videoQuality || {};
  const playBackSpeed = [
    { label: '0.25', value: 0.25 },
    { label: '0.5', value: 0.5 },
    { label: '0.75', value: 0.75 },
    { label: 'Normal', value: 1 },
    { label: '1.25', value: 1.25 },
    { label: '1.5', value: 1.5 },
    { label: '1.75', value: 1.75 },
    { label: '2', value: 2 },
  ];
  return (
    <View style={styles.selectQuality}>
      <View style={styles.width1_4}>
        <Text style={styles.selectTextHeader}>Qualities</Text>
        <ScrollView>
          {videoQualities.map((element) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  dispatch({
                    type: 'SET_VIDEO_QUALITY',
                    videoQuality: element,
                  });
                }}
                key={`index${element.label}`}
                style={styles.qualityDetail}
              >
                {element.height === videoQuality.height ? (
                  <IconFontAwesome
                    style={styles.backward}
                    name={'check-circle-o'}
                    size={14}
                  />
                ) : (
                  <View style={{ width: 22.5 }} />
                )}

                <Text
                  style={{
                    fontSize: 12,
                    color: '#FFF',
                  }}
                >
                  {element.label}
                </Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
      <View style={styles.width1_4}>
        <Text style={styles.selectTextHeader}>Speed</Text>
        <ScrollView>
          {playBackSpeed.map((element) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  dispatch({
                    type: 'SET_PLAYBACK_SPEED',
                    playbackSpeed: element.value,
                  });
                }}
                key={`index${element.value}`}
                style={styles.qualityDetail}
              >
                {element.value === state.playbackSpeed ? (
                  <IconFontAwesome
                    style={styles.backward}
                    name={'check-circle-o'}
                    size={14}
                  />
                ) : (
                  <View style={{ width: 22.5 }} />
                )}

                <Text
                  style={{
                    fontSize: 12,
                    color: '#FFF',
                  }}
                >
                  {element.label}
                </Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

export default SelectView;
