## jixie-player-sdk (for React Native apps)

- This is a react native SDK to integrate Jixie video player into your react native application to stream videos from Jixie infrastructure.
- You can also use the Android Native SDK (see 'android native' branch) to integrate Jixie video player into your Android application.

## Installation

```sh
npm install jixie-player-sdk react-native-video-inc-ads react-native-vector-icons
```
or
```
yarn add jixie-player-sdk react-native-video-inc-ads react-native-vector-icons
```

Then, for React Native >= 0.60:
```
cd ios
pod install
```

Add the following at the beginning of `./android/app/build.gradle` on Android (required for `react-native-vector-icons` to work):
```gradle
apply from: "../../node_modules/react-native-vector-icons/fonts.gradle"
```

Add the following in your `Info.plist` file on iOS (required for `react-native-vector-icons` to work):

```xml
<key>UIAppFonts</key>
<array>
	<string>MaterialIcons.ttf</string>
    <string>FontAwesome.ttf</string>
</array>
```
For React Native < 0.60
```
react-native link react-native-video
react-native link react-native-vector-icons
```

## Usage

```js
import VideoPlayer from 'jixie-player-sdk';
import React, { useRef} from 'react';
import { Button } from 'react-native';

const videoRef = useRef(null);

render() {
  
  return(
    <View>
      <Button title={'play'} onPress={() =>{videoRef.current.play()}
      }}>
        
      <VideoPlayer
        ref={videoRef}
        accountId={'accountId'}
        clientId={'clientId'}
        videoWidth={428}
        videos={['16459, 16451']}
        mainAdUnit={{ id: 'id', delay: 'delay' }}
        onStart={() => {
          // trigger when video start 
        }}
        onEnded={() => {
          // trigger when video end
        }}
        onVideoChange={() => {
          // trigger when video change
        }}
        onPlaying={() => {
          // trigger when video playing
        }}
      />
    </View>
    
  )
}


```
## Props

| Prop             | Required    | Type     | Description                                                                                                 |
|------------------|-------------|----------|-------------------------------------------------------------------------------------------------------------|
| accountId        | true        | string   | that parameter is important as it allows to identify the organisation who has integrated the video player   |
| jixieAccessKey   | true        | string   | the access key provided for Jixie's partner to authenticate API requests, for example get DRM licence token |
| clientId         | true        | string   | user ad identifier or the client_id we have stored locally                                                  |
| videoWidth       | false       | number   | width of the video to calculate the player size.                                                            |
| videos           | true        | array    | an array of videoIds                                                                                        |
| mainAdUnit       | false       | object   | an object wrap id and delay                                                                                 |
|  -id             | false       | string   | an identifier that will allow the player to pass a proper request to the ad-server, used for the ad-slot    |
|                  |             |          | that is at the beginning of the video. If not set then there is no ad played                                |
|  -delay          | false       | string   | a delay in seconds to make the ad call after the start of the video.                                        | 
| adMidRoll        | false       | object   | an object wrap id and interval, minimum, maximum                                                            |
|  -id             | false       | number   | id of the ad unit used for mid roll ads. If not set then there is no mid-roll ads                           |
|  -interval       | false       | number   | it is the interval in seconds between 2 calls to the ad-server in case there is a mid-roll ad unit          |
|  -Minimum        | false       | number   | Minimum time left before the end of the video                                                               |
|  -Maximum        | false       | number   | maximum number of ads for a video                                                                           |
| hotspot          | false       | object   | an object wrap id, hsDelay, hsDuration, maximum                                                             |
|  -id             | false       | number   | the identifier of the hotspot ad unit                                                                       |
|  -hsDelay        | false       | number   | the delay between a video ad and a hotspot
|  -hsDuration     | false       | number   | the duration to display the hotspot
|  -maximum        | false       | number   | Maximum number of hotspots

## Function

load()

play()

pause()

nextVideo()

## Event props
onStart

Callback function that is called when the player start video.

onPlaying

Callback function that is called every progressUpdateInterval seconds with info about which position the media is currently playing.

onEnded

Callback function that is called when the player reaches the end of the video.

onVideoChange

Callback function that is called when the player next video.

onMetadata

Callback function to receive metadata.

onEvent

trigger all events.
