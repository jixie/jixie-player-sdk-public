/**
 * @param {number} id: an identifier that will allow the player to pass a proper request to the ad-server, used for the ad-slot that is atthe beginning of the video. If not set then there is no ad played
 * @param {number} delay: a delay in seconds to make the ad call after the start of the video.
 */
export interface AdPreRoll {
    id: number;
    delay: number;
}
/**
 * @param {number} id: ID of the ad unit used for mid roll ads. If not set then there is no mid-roll ads
 * @param {number} interval: it is the interval in seconds between 2 calls to the ad-server in case there is a mid-roll ad unit
 * @param {number} minimum: in seconds, indicates if we can play a mid-roll. Indeed, we don’t play a mid-roll if the remaining time in the video is below that number.
 * @param {number} maximum: maximum number of ads for a video
 */
export interface adMidRoll {
    id: number;
    interval: number;
    minimum: number;
    maximum: number;
}
/**
 * @param {number} id: the identifier of the hotspot ad unit. If not set there is no hotspot
 * @param {number} hsDelay: the delay between a video ad and a hotspot
 * @param {number} hsDuration: the duration to display the hotspot
 * @param {number} maximum: number of hotspots
 */
export interface Hotspot {
    id: number;
    hsDelay: number;
    hsDuration: number;
    maximum: number;
}
