/**
 *
 * @param str
 * @param qualities
 * @return {{rendition: (string|string), label: (string|*), quality}|{rendition: string, label: (string|string), quality: *}}
 */
export const handleRendition = (str, qualities) => {
  const i = str.indexOf('master-');
  const screenWidth = i === -1 ? 'master' : str.substr(i + 7).replace('.m3u8', '');
  const quality = qualities.find(quality => {
    return quality.label === screenWidth;
  });

  if (screenWidth === 'master') {
    const lastQuality = qualities.length > 0 ? qualities.length - 1 : 0;
    const rendition = qualities[lastQuality] === 0 ? '' : `${qualities[lastQuality].width}x${qualities[lastQuality].height}`;
    const label = qualities[lastQuality] === 0 ? '' : qualities[lastQuality].label;
    return {
      rendition: rendition,
      quality: qualities[lastQuality],
      label: label
    };
  }

  return {
    rendition: `${quality.width}x${quality.height}`,
    quality: quality,
    label: screenWidth
  };
};
//# sourceMappingURL=handleRendition.js.map