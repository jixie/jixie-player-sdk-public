import React, { useContext, createContext } from 'react';
import axios from 'axios';
const AxiosContext = /*#__PURE__*/createContext(axios);
/**
 * @param axios
 * @param children
 * @constructor
 */

export const Provider = _ref => {
  let {
    axios,
    children
  } = _ref;
  return /*#__PURE__*/React.createElement(AxiosContext.Provider, {
    value: axios
  }, children);
};
/**
 * @return <Promise> return a promise used token as AxiosInstance.
 */

export function useApiWithToken() {
  const axiosInstance = useContext(AxiosContext);
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer token`;
  return axiosInstance;
}
/**
 * @return <Promise> return a promise as AxiosInstance.
 */

export function useApi() {
  return useContext(AxiosContext);
}
//# sourceMappingURL=axios.js.map