import { useCallback, useState } from 'react';
import { useApi } from '../service/axios';
/**
 * @return {[{stream: {}, pending: boolean, error: {}}, {getVideo: ((function(): void)|*)}]}
 */

const useStream = () => {
  const [pending, setPending] = useState(false);
  const [stream, setStream] = useState({});
  const [error, setError] = useState({});
  const axios = useApi();
  const getVideo = useCallback(url => {
    setPending(true);
    axios.get(url).then(result => {
      setStream(result.data);
    }).catch(error => {
      setError(error === null || error === void 0 ? void 0 : error.response);
    }).finally(() => {
      setPending(false);
    });
  }, [axios]);
  return [{
    stream,
    error,
    pending
  }, {
    getVideo
  }];
};

export default useStream;
//# sourceMappingURL=useStream.js.map