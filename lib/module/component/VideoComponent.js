import React, { useRef, useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import Video, { DRMType } from 'react-native-video-inc-ads';
import { styles } from '../Styles';
import SeekBar from './SeekBar';
import { getDurationTime } from '../handleTime';
import { useSDK } from '../context/SDKContext';
import { handleVideoSize } from '../handleVideoSize';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import SelectView from './SelectView';
/**
 * @description the main video playing component with basic action controls.
 * @param props
 * @return {JSX.Element}
 * @constructor
 */

const VideoComponent = props => {
  var _state$currentVideoIn, _state$currentVideoIn2, _state$currentVideoIn3, _state$currentVideoIn4, _state$currentVideoIn5, _state$currentVideoIn6, _state$currentVideoIn7, _state$currentVideoIn8;

  const {
    state,
    dispatch
  } = useSDK();
  const timeRef = useRef(null);
  const playerRef = useRef(null);
  const [seekTouchStart, setSeekTouchStart] = useState(0);
  const [seekProgressStart, setSeekProgressStart] = useState(0);
  const [seekBarWidth, setSeekBarWidth] = useState(200);
  const [wasPlayingBeforeSeek, setWasPlayingBeforeSeek] = useState(false);
  const [isAdPlay, setIsAsPlay] = useState(false);
  /**
   * get seek UI property
   * @param nativeEvent
   */

  const onSeekBarLayout = nativeEvent => {
    const padding = 20;
    setSeekBarWidth(nativeEvent.layout.width - padding);
  };
  /**
   * handle seek finish
   * @param e
   */


  const onSeekGrant = e => {
    setSeekTouchStart(e.nativeEvent.pageX);
    setSeekProgressStart(state.progress);
    setWasPlayingBeforeSeek(state.isPlaying);
    dispatch({
      type: 'ON_SEEK_GRANT',
      isSeeking: true,
      isPlaying: false
    });
  };
  /**
   * handle seek
   * @param e
   */


  const onSeek = e => {
    const diff = e.nativeEvent.pageX - seekTouchStart;
    const ratio = 100 / seekBarWidth;
    const progress = seekProgressStart + ratio * diff / 100;
    dispatch({
      type: 'ON_SEEK',
      progress: progress
    });
    playerRef.current.seek(progress * state.duration);
  };
  /**
   * handle seek 15s
   * @param time
   */


  const onSeek15s = time => {
    const {
      progress,
      duration
    } = state;
    dispatch({
      type: 'ON_SEEK',
      progress: progress
    });
    playerRef.current.seek(duration * progress + time);
  };
  /**
   * handle seek release
   */


  const onSeekRelease = () => {
    dispatch({
      type: 'ON_SEEK_RELEASE',
      isSeeking: false,
      isPlaying: wasPlayingBeforeSeek
    });
    props.showControls();
  };
  /**
   * sync video playing progress,
   * prepare next video
   * @param event
   */


  function onProgress(event) {
    var _timeRef$current;

    if (isAdPlay) {
      return;
    }

    if (state.isSeeking) {
      return;
    }

    if (props.onProgress) {
      props.onProgress(event);
    }

    dispatch({
      type: 'ON_PROGRESS',
      progress: event.currentTime / (props.duration || state.duration)
    });
    (_timeRef$current = timeRef.current) === null || _timeRef$current === void 0 ? void 0 : _timeRef$current.setNativeProps({
      text: getDurationTime(event.currentTime)
    });
  }
  /**
   * handle seek icon to start point
   * @param event
   */


  const onEnd = event => {
    var _timeRef$current2;

    if (props.onEnd) {
      props.onEnd(event);
    }

    dispatch({
      type: 'ON_END',
      progress: 1
    });

    if (!props.loop) {
      playerRef && playerRef.current.seek(0);
    } else {
      playerRef.current.seek(0);
    }

    (_timeRef$current2 = timeRef.current) === null || _timeRef$current2 === void 0 ? void 0 : _timeRef$current2.setNativeProps({
      text: getDurationTime(state.duration)
    });
  };
  /**
   * handle load event
   * @param event
   */


  const onLoad = event => {
    if (props.onLoad) {
      props.onLoad(event);
    }

    const {
      duration
    } = event;
    dispatch({
      type: 'ON_LOAD',
      duration: duration
    });
  };
  /**
   * handle mute action
   */


  const onMutePress = () => {
    const isMuted = !state.isMuted;

    if (props.onMutePress) {
      props.onMutePress(isMuted);
    }

    dispatch({
      type: 'ON_MUTE',
      isMuted: isMuted
    });
    props.showControls();
  };
  /**
   * handle play action
   */


  const onPlayPress = () => {
    if (props.onPlayPress) {
      props.onPlayPress();
    }

    dispatch({
      type: 'ON_PLAY',
      isPlaying: !state.isPlaying
    });
    props.showControls();
  };
  /**
   * handle update time on time indicator
   * @param e
   */


  const onSeekEvent = e => {
    var _timeRef$current3;

    (_timeRef$current3 = timeRef.current) === null || _timeRef$current3 === void 0 ? void 0 : _timeRef$current3.setNativeProps({
      text: getDurationTime(e.currentTime)
    });
  };
  /**
   * handle layout display video and VAST
   * @param e
   */


  const adEvent = e => {
    switch (e.event) {
      case 'STARTED':
        return setIsAsPlay(true);

      case 'ALL_ADS_COMPLETED':
      case 'COMPLETED':
      case 'SKIPPED':
        return setIsAsPlay(false);
    }
  };

  const {
    style,
    pauseOnPress,
    nextVideo
  } = props;
  /**
   * @description controls bar
   * @return {JSX.Element}
   */

  const renderControls = () => {
    return /*#__PURE__*/React.createElement(View, {
      style: [styles.controls]
    }, /*#__PURE__*/React.createElement(SeekBar, {
      disableSeek: props.disableSeek,
      fullWidth: false,
      onSeek: onSeek,
      onSeekBarLayout: onSeekBarLayout,
      onSeekGrant: onSeekGrant,
      onSeekRelease: onSeekRelease
    }), /*#__PURE__*/React.createElement(View, {
      style: styles.videoControls
    }, /*#__PURE__*/React.createElement(View, {
      style: {
        flexDirection: 'row',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: onPlayPress
    }, /*#__PURE__*/React.createElement(Icon, {
      style: [styles.playControl],
      name: state.isPlaying ? 'pause' : 'play-arrow',
      size: 30
    })), /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        nextVideo();
      }
    }, /*#__PURE__*/React.createElement(Icon, {
      style: [styles.playControl],
      name: 'skip-next',
      size: 30
    })), /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        onSeek15s(-15);
      }
    }, /*#__PURE__*/React.createElement(IconFontAwesome, {
      style: [styles.backward],
      name: 'backward',
      size: 18
    })), /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        onSeek15s(15);
      }
    }, /*#__PURE__*/React.createElement(IconFontAwesome, {
      style: [styles.forward],
      name: 'forward',
      size: 18
    })), /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: () => {
        dispatch({
          type: 'CHANGE_QUALITY_MODAL_STATE',
          state: !state.selectModalIsShow
        });
      }
    }, /*#__PURE__*/React.createElement(Icon, {
      name: 'settings',
      size: 20,
      color: '#FFF'
    }))), props.showDuration && /*#__PURE__*/React.createElement(View, {
      style: {
        flexDirection: 'row',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(TextInput, {
      style: [styles.durationText],
      editable: false,
      ref: timeRef,
      value: getDurationTime(0)
    }), /*#__PURE__*/React.createElement(Text, {
      style: [styles.durationText]
    }, " / "), /*#__PURE__*/React.createElement(Text, {
      style: [styles.durationText]
    }, getDurationTime(state.duration))), props.muted ? null : /*#__PURE__*/React.createElement(TouchableOpacity, {
      onPress: onMutePress
    }, /*#__PURE__*/React.createElement(Icon, {
      style: [styles.extraControl],
      name: state.isMuted ? 'volume-off' : 'volume-up',
      size: 24
    }))));
  };

  return /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Video, {
    onError: error => {
      props.onVideoError(error);
    },
    selectedVideoTrack: {
      type: 'resolution',
      value: state === null || state === void 0 ? void 0 : (_state$currentVideoIn = state.currentVideoInfo) === null || _state$currentVideoIn === void 0 ? void 0 : (_state$currentVideoIn2 = _state$currentVideoIn.videoQuality) === null || _state$currentVideoIn2 === void 0 ? void 0 : _state$currentVideoIn2.height
    },
    rate: state.playbackSpeed ? state.playbackSpeed : 1,
    adTagUrl: props.adTagUrl,
    onReceiveAdEvent: event => {
      props === null || props === void 0 ? void 0 : props.onReceiveAdEvent(event);
      adEvent(event);
    },
    style: [styles.video, handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn3 = state.currentVideoInfo) === null || _state$currentVideoIn3 === void 0 ? void 0 : _state$currentVideoIn3.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn4 = state.currentVideoInfo) === null || _state$currentVideoIn4 === void 0 ? void 0 : _state$currentVideoIn4.videoQuality.height, state.width), style, {
      zIndex: isAdPlay ? 10 : 0
    }],
    ref: playerRef,
    muted: props.muted || state.isMuted,
    paused: !state.isPlaying,
    onProgress: onProgress,
    onEnd: onEnd,
    onLoad: onLoad,
    source: props.source,
    resizeMode: 'contain',
    onSeek: onSeekEvent,
    drm: props.drmToken ? {
      type: DRMType.WIDEVINE,
      licenseServer: 'https://693e0978.drm-widevine-licensing.axprod.net/AcquireLicense',
      headers: {
        'X-AxDRM-Message': props.drmToken
      }
    } : {}
  }), /*#__PURE__*/React.createElement(View, {
    style: [handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn5 = state.currentVideoInfo) === null || _state$currentVideoIn5 === void 0 ? void 0 : _state$currentVideoIn5.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn6 = state.currentVideoInfo) === null || _state$currentVideoIn6 === void 0 ? void 0 : _state$currentVideoIn6.videoQuality.height, state.width), {
      marginTop: -handleVideoSize(state === null || state === void 0 ? void 0 : (_state$currentVideoIn7 = state.currentVideoInfo) === null || _state$currentVideoIn7 === void 0 ? void 0 : _state$currentVideoIn7.videoQuality.width, state === null || state === void 0 ? void 0 : (_state$currentVideoIn8 = state.currentVideoInfo) === null || _state$currentVideoIn8 === void 0 ? void 0 : _state$currentVideoIn8.videoQuality.height, state.width).height
    }]
  }, /*#__PURE__*/React.createElement(TouchableOpacity, {
    style: styles.overlayButton,
    onPress: () => {
      props.showControls();
      if (pauseOnPress) onPlayPress();
    }
  })), !state.isPlaying || state.isControlsVisible || state.selectModalIsShow ? renderControls() : /*#__PURE__*/React.createElement(SeekBar, {
    fullWidth: true // customStyles={customStyles}
    ,
    disableSeek: props.disableSeek,
    onSeek: onSeek,
    onSeekBarLayout: onSeekBarLayout,
    onSeekGrant: onSeekGrant,
    onSeekRelease: onSeekRelease
  }), state.selectModalIsShow && /*#__PURE__*/React.createElement(SelectView, null));
};

export default VideoComponent;
//# sourceMappingURL=VideoComponent.js.map