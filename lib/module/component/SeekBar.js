import React from 'react';
import { View } from 'react-native';
import { styles } from '../Styles';
import PropTypes from 'prop-types';
import { useSDK } from '../context/SDKContext';
/**
 * @description seek bar component
 * @param props
 * @return {JSX.Element}
 * @constructor
 */

const SeekBar = props => {
  const onSeek = e => {
    props.onSeek(e);
  };

  const onSeekStartResponder = () => {
    return true;
  };

  const onSeekMoveResponder = () => {
    return true;
  };

  const {
    state
  } = useSDK();

  const onSeekGrant = e => {
    props.onSeekGrant(e);
  };

  const onSeekRelease = () => {
    props.onSeekRelease();
  };

  const onSeekBarLayout = _ref => {
    let {
      nativeEvent
    } = _ref;
    props.onSeekBarLayout(nativeEvent);
  };

  const {
    progress,
    isSeeking
  } = state;

  const handleProgress = progress => {
    if (progress === Infinity || isNaN(progress)) {
      return 0.00001;
    }

    return progress;
  };

  const {
    fullWidth,
    disableSeek
  } = props;
  return /*#__PURE__*/React.createElement(View, {
    style: [styles.seekBar, fullWidth ? styles.seekBarFullWidth : {} // customStyles.seekBar,
    // fullWidth ? customStyles.seekBarFullWidth : {},
    ],
    onLayout: onSeekBarLayout
  }, /*#__PURE__*/React.createElement(View, {
    style: [{
      flexGrow: handleProgress(progress)
    }, styles.seekBarProgress // customStyles.seekBarProgress,
    ]
  }), !fullWidth && !disableSeek ? /*#__PURE__*/React.createElement(View, {
    style: [styles.seekBarKnob, isSeeking ? {
      transform: [{
        scale: 1
      }]
    } : {}],
    hitSlop: {
      top: 20,
      bottom: 20,
      left: 10,
      right: 20
    },
    onStartShouldSetResponder: onSeekStartResponder,
    onMoveShouldSetPanResponder: onSeekMoveResponder,
    onResponderGrant: onSeekGrant,
    onResponderMove: onSeek,
    onResponderRelease: onSeekRelease,
    onResponderTerminate: onSeekRelease
  }) : null, /*#__PURE__*/React.createElement(View, {
    style: [styles.seekBarBackground, {
      flexGrow: 1 - handleProgress(progress)
    }]
  }));
};

SeekBar.propTypes = {
  fullWidth: PropTypes.bool,
  customStyles: PropTypes.any,
  onSeek: PropTypes.func
};
export default SeekBar;
//# sourceMappingURL=SeekBar.js.map