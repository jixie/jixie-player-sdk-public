import { createContext, useContext } from 'react';
export const SDKContext = /*#__PURE__*/createContext({});
export function useSDK() {
  return useContext(SDKContext);
}
//# sourceMappingURL=SDKContext.js.map