"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

var _axios = require("../service/axios");

const useTrack = () => {
  const [pending, setPending] = (0, _react.useState)(false);
  const [errorMessage, setErrorMessage] = (0, _react.useState)('');
  const [result, setResult] = (0, _react.useState)();
  const slackApi = 'https://hooks.slack.com/services/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';
  const slackHeader = {
    'Content-type': 'application/json'
  };
  const axios = (0, _axios.useApi)();
  const tracker = (0, _react.useCallback)((params, aggregateTracking) => {
    const stringParams = {
      blocks: [{
        type: 'divider'
      }, {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `*${params.action}* 
              videoid: ${params.videoid}
              elapsedms :${params.elapsedms}
              vposition: ${params.vposition}
              autoplay: ${params.autoplay} 
              startmode: ${params.startmode} 
              volume: ${params.volume}
              playhead: ${params.playhead} 
              rendition: ${params.rendition}
              viewability: ${params.viewability}
              origtech: ${params.origtech}
              realtech: ${params.realtech}
              accountid: ${params.accountid}
              ownerid: ${params.ownerid}
              device: ${params.device}
              p_domain: ${params.p_domain}
              domain: ${params.domain}
              pageurl: ${params.pageurl}
              browser: ${params.browser} 
              os: ${params.os}
              client_id: ${params.client_id}
              session_id: ${params.session_id} 
              errormessage: ${params.errormessage || ''}
              errorcode: ${params.errorcode || ''}
              adTagUrl: ${params.adTagUrl || ''}
              ${params.step ? 'step: 5' : ''}
              `
        }
      }]
    };
    const aggregateParams = {
      blocks: [{
        type: 'divider'
      }, {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `action: duragg
              accountid: ${params.accountid}
              ownerid: ${params.ownerid}
              rendition: ${params.rendition}
              videoid: ${params.videoid}
              step: ${params.step}`
        }
      }]
    };
    setPending(true);
    axios.get('https://traid.jixie.io/sync/video', {
      params
    }).then(result => {
      axios.post(slackApi, aggregateTracking ? aggregateParams : stringParams, {
        headers: slackHeader
      }).finally(errorMessage => errorMessage);
      setResult(result.data);
    }).catch(error => {
      var _error$response, _error$response$data;

      setErrorMessage(error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : (_error$response$data = _error$response.data) === null || _error$response$data === void 0 ? void 0 : _error$response$data.message);
    }).finally(() => {
      setPending(false);
    });
  }, [axios]);
  return [{
    result,
    errorMessage,
    pending
  }, {
    tracker
  }];
};

var _default = useTrack;
exports.default = _default;
//# sourceMappingURL=useTrack.js.map