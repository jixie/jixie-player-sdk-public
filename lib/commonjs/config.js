"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.config = void 0;
const config = {
  axios: {
    baseURL: 'https://jx-dam-api-express.azurewebsites.net/',
    timeout: 20000
  }
};
exports.config = config;
//# sourceMappingURL=config.js.map