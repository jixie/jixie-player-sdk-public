"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleVideoSize = void 0;

var _reactNative = require("react-native");

const {
  width
} = _reactNative.Dimensions.get('window');

const handleVideoSize = (videoWidth, videoHeight, widthOverride) => {
  const _width = widthOverride ? widthOverride : width;

  const ratio = _width / videoWidth;
  return {
    height: videoHeight * ratio,
    width: _width
  };
};

exports.handleVideoSize = handleVideoSize;
//# sourceMappingURL=handleVideoSize.js.map