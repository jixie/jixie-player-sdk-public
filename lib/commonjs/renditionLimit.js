"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renditionLimit = void 0;

const renditionLimit = (ratio, qualities) => {
  const _ratio = Math.round(ratio * 10);

  if (_ratio === Math.round(9 / 16 * 10)) {
    return [{
      label: '240p',
      width: 426,
      height: 240
    }, {
      label: '360p',
      width: 640,
      height: 360
    }, {
      label: '480p',
      width: 852,
      height: 480
    }, {
      label: '720p',
      width: 1280,
      height: 720
    }];
  }

  if (ratio === 1) {
    return [[{
      label: '240p',
      width: 426,
      height: 240
    }, {
      label: '360p',
      width: 640,
      height: 360
    }, {
      label: '480p',
      width: 852,
      height: 480
    }]];
  }

  if (_ratio === Math.round(16 / 9 * 10)) {
    return [{
      label: '240p',
      width: 426,
      height: 240
    }, {
      label: '360p',
      width: 640,
      height: 360
    }];
  } else {
    return qualities;
  }
};

exports.renditionLimit = renditionLimit;
//# sourceMappingURL=renditionLimit.js.map