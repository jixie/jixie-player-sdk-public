"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = VideoPlayer;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactNative = require("react-native");

var _Styles = require("./Styles");

var _reactNativeVideo = _interopRequireDefault(require("react-native-video"));

var _axios = require("./service/axios");

var _axios2 = _interopRequireDefault(require("axios"));

var _config = require("./config");

var _StartButton = _interopRequireDefault(require("./component/StartButton"));

var _SDKContext = require("./context/SDKContext");

var _SDKReducer = require("./context/SDKReducer");

var _VideoComponent = _interopRequireDefault(require("./component/VideoComponent"));

var _ControlsBar = require("./component/ControlsBar");

var _handleVideoSize = require("./handleVideoSize");

var _useVideo = _interopRequireDefault(require("./hooks/useVideo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

let ViewPropTypesVar;

if (_reactNative.ViewPropTypes) {
  ViewPropTypesVar = _reactNative.ViewPropTypes;
} else {
  ViewPropTypesVar = _reactNative.View.propTypes;
}

function VideoPlayer(props) {
  const initialState = {
    isStarted: props.autoplay,
    isPlaying: props.autoplay,
    isMuted: props.defaultMuted,
    isControlsVisible: !props.hideControlsOnStart,
    hasEnded: false,
    progress: 0,
    width: 200,
    duration: 0,
    isSeeking: false
  };
  const [state, dispatch] = (0, _react.useReducer)(_SDKReducer.reducer, initialState);
  const [{
    data,
    errorMessage,
    pending
  }, {
    getVideo
  }] = (0, _useVideo.default)();
  (0, _react.useEffect)(() => {
    getVideo();
  }, []); // console.log(data, errorMessage, pending);

  function onLayout(event) {
    const {
      width
    } = event.nativeEvent.layout;
    dispatch({
      type: 'SET_LAYOUT',
      width: width
    });
  }

  let controlsTimeout = null;
  (0, _react.useEffect)(() => {
    if (props.autoplay) {
      hideControls();
    }

    if (controlsTimeout) {
      controlsTimeout = null;
      return clearTimeout(controlsTimeout);
    }
  }, []);

  function hideControls() {
    if (props.onHideControls) {
      props.onHideControls();
    }

    if (props.disableControlsAutoHide) {
      return;
    }

    if (controlsTimeout) {
      clearTimeout(controlsTimeout);
      controlsTimeout = null;
    }

    controlsTimeout = setTimeout(() => {
      dispatch({
        type: 'HIDE_CONTROLS',
        isControlsVisible: false
      });
    }, props.controlsTimeout);
  }

  const showControls = () => {
    if (props.onShowControls) {
      props.onShowControls();
    }

    dispatch({
      type: 'SHOW_CONTROLS',
      isControlsVisible: true
    });
    hideControls();
  };

  function onStartPress() {
    console.log('halo'); //if ad-unit set

    if (props.onStart) {
      props.onStart();
    }

    dispatch({
      type: 'ON_START',
      isPlaying: true,
      isStarted: true,
      hasEnded: false,
      progress: state.progress === 1 ? 0 : state.progress
    });
    hideControls();
  }

  function renderContent() {
    const {
      style
    } = props;
    const {
      isStarted
    } = state;

    if (!isStarted) {
      return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
        style: [_Styles.styles.preloadingPlaceholder, (0, _handleVideoSize.handleVideoSize)(state.width), style]
      }, /*#__PURE__*/_react.default.createElement(_ControlsBar.ControlsBar, null), /*#__PURE__*/_react.default.createElement(_StartButton.default, {
        onStartPress: onStartPress,
        customStyles: props.customStyles
      }));
    }

    return /*#__PURE__*/_react.default.createElement(_VideoComponent.default, {
      customStyles: props.customStyles,
      showControls: showControls,
      video: props.video,
      showDuration: props.showDuration
    });
  }

  return /*#__PURE__*/_react.default.createElement(_axios.Provider, {
    axios: _axios2.default.create(_config.config.axios)
  }, /*#__PURE__*/_react.default.createElement(_SDKContext.SDKContext.Provider, {
    value: {
      state,
      dispatch
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    onLayout: onLayout,
    style: props.customStyles.wrapper
  }, renderContent())));
}

VideoPlayer.propTypes = {
  accountID: _propTypes.default.number,
  quality: _propTypes.default.string,
  id: _propTypes.default.number,
  appName: _propTypes.default.string,
  pageName: _propTypes.default.string,
  adPreRoll: _propTypes.default.shape({
    id: _propTypes.default.number,
    delay: _propTypes.default.number
  }),
  adMidRoll: _propTypes.default.shape({
    id: _propTypes.default.number,
    interval: _propTypes.default.number,
    minimum: _propTypes.default.number,
    maximum: _propTypes.default.number
  }),
  Hotspot: _propTypes.default.shape({
    id: _propTypes.default.number,
    hsDelay: _propTypes.default.number,
    hsDuration: _propTypes.default.number,
    maximum: _propTypes.default.number
  }),
  video: _reactNativeVideo.default.propTypes.source,
  videoWidth: _propTypes.default.number,
  videoHeight: _propTypes.default.number,
  duration: _propTypes.default.number,
  autoplay: _propTypes.default.bool,
  paused: _propTypes.default.bool,
  defaultMuted: _propTypes.default.bool,
  muted: _propTypes.default.bool,
  style: ViewPropTypesVar.style,
  controlsTimeout: _propTypes.default.number,
  disableControlsAutoHide: _propTypes.default.bool,
  disableFullscreen: _propTypes.default.bool,
  loop: _propTypes.default.bool,
  resizeMode: _reactNativeVideo.default.propTypes.resizeMode,
  hideControlsOnStart: _propTypes.default.bool,
  disableSeek: _propTypes.default.bool,
  pauseOnPress: _propTypes.default.bool,
  fullScreenOnLongPress: _propTypes.default.bool,
  customStyles: _propTypes.default.shape({
    wrapper: ViewPropTypesVar.style,
    video: _reactNativeVideo.default.propTypes.style,
    videoWrapper: ViewPropTypesVar.style,
    controls: ViewPropTypesVar.style,
    playControl: ViewPropTypesVar.style,
    controlButton: ViewPropTypesVar.style,
    seekBar: ViewPropTypesVar.style,
    seekBarFullWidth: ViewPropTypesVar.style,
    seekBarProgress: ViewPropTypesVar.style,
    seekBarKnob: ViewPropTypesVar.style,
    seekBarKnobSeeking: ViewPropTypesVar.style,
    seekBarBackground: ViewPropTypesVar.style,
    playButton: ViewPropTypesVar.style,
    durationText: ViewPropTypesVar.style
  }),
  onLoad: _propTypes.default.func,
  onPlayPress: _propTypes.default.func,
  onHideControls: _propTypes.default.func,
  onShowControls: _propTypes.default.func,
  onMutePress: _propTypes.default.func,
  showDuration: _propTypes.default.bool,
  onEnd: _propTypes.default.func,
  onProgress: _propTypes.default.func,
  onStart: _propTypes.default.func,
  onVideoChange: _propTypes.default.func
};
VideoPlayer.defaultProps = {
  autoplay: false,
  controlsTimeout: 2000,
  loop: false,
  resizeMode: 'contain',
  disableSeek: false,
  pauseOnPress: false,
  fullScreenOnLongPress: false,
  customStyles: {},
  showDuration: false
};
//# sourceMappingURL=index.js.map
