"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactNative = require("react-native");

var _Styles = require("../Styles");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _SDKContext = require("../context/SDKContext");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @description seek bar component
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
const SeekBar = props => {
  const onSeek = e => {
    props.onSeek(e);
  };

  const onSeekStartResponder = () => {
    return true;
  };

  const onSeekMoveResponder = () => {
    return true;
  };

  const {
    state
  } = (0, _SDKContext.useSDK)();

  const onSeekGrant = e => {
    props.onSeekGrant(e);
  };

  const onSeekRelease = () => {
    props.onSeekRelease();
  };

  const onSeekBarLayout = _ref => {
    let {
      nativeEvent
    } = _ref;
    props.onSeekBarLayout(nativeEvent);
  };

  const {
    progress,
    isSeeking
  } = state;

  const handleProgress = progress => {
    if (progress === Infinity || isNaN(progress)) {
      return 0.00001;
    }

    return progress;
  };

  const {
    fullWidth,
    disableSeek
  } = props;
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [_Styles.styles.seekBar, fullWidth ? _Styles.styles.seekBarFullWidth : {} // customStyles.seekBar,
    // fullWidth ? customStyles.seekBarFullWidth : {},
    ],
    onLayout: onSeekBarLayout
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [{
      flexGrow: handleProgress(progress)
    }, _Styles.styles.seekBarProgress // customStyles.seekBarProgress,
    ]
  }), !fullWidth && !disableSeek ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [_Styles.styles.seekBarKnob, isSeeking ? {
      transform: [{
        scale: 1
      }]
    } : {}],
    hitSlop: {
      top: 20,
      bottom: 20,
      left: 10,
      right: 20
    },
    onStartShouldSetResponder: onSeekStartResponder,
    onMoveShouldSetPanResponder: onSeekMoveResponder,
    onResponderGrant: onSeekGrant,
    onResponderMove: onSeek,
    onResponderRelease: onSeekRelease,
    onResponderTerminate: onSeekRelease
  }) : null, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [_Styles.styles.seekBarBackground, {
      flexGrow: 1 - handleProgress(progress)
    }]
  }));
};

SeekBar.propTypes = {
  fullWidth: _propTypes.default.bool,
  customStyles: _propTypes.default.any,
  onSeek: _propTypes.default.func
};
var _default = SeekBar;
exports.default = _default;
//# sourceMappingURL=SeekBar.js.map