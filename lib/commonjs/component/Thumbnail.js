"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Styles = require("../Styles");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _MaterialIcons = _interopRequireDefault(require("react-native-vector-icons/MaterialIcons"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const BackgroundImage = _reactNative.ImageBackground || _reactNative.Image; // fall back to Image if RN < 0.46

class Thumbnail extends _react.Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      width: 200,
      height: 200
    });
  }

  getSizeStyles() {
    const {
      videoWidth,
      videoHeight
    } = this.props;
    const {
      width
    } = this.state;
    const ratio = videoHeight / videoWidth;
    return {
      height: width * ratio,
      width
    };
  }

  render() {
    const {
      onStartPress,
      style,
      customStyles,
      ...props
    } = this.props;
    return /*#__PURE__*/_react.default.createElement(BackgroundImage, _extends({}, props, {
      style: [_Styles.styles.thumbnail, this.getSizeStyles(), style, customStyles.thumbnail],
      source: this.props.thumbnail
    }), /*#__PURE__*/_react.default.createElement(_reactNative.TouchableOpacity, {
      style: [_Styles.styles.playButton, customStyles.playButton],
      onPress: onStartPress
    }, /*#__PURE__*/_react.default.createElement(_MaterialIcons.default, {
      style: [_Styles.styles.playArrow, customStyles.playArrow],
      name: "play-arrow",
      size: 42
    })));
  }

}

exports.default = Thumbnail;
//# sourceMappingURL=Thumbnail.js.map