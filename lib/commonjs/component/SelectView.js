"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Styles = require("../Styles");

var _reactNative = require("react-native");

var _SDKContext = require("../context/SDKContext");

var _FontAwesome = _interopRequireDefault(require("react-native-vector-icons/FontAwesome"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const SelectView = () => {
  var _state$currentVideoIn, _state$currentVideoIn2;

  const {
    state,
    dispatch
  } = (0, _SDKContext.useSDK)();
  const videoQualities = (state === null || state === void 0 ? void 0 : (_state$currentVideoIn = state.currentVideoInfo) === null || _state$currentVideoIn === void 0 ? void 0 : _state$currentVideoIn.videoQualities) || [];
  const videoQuality = (state === null || state === void 0 ? void 0 : (_state$currentVideoIn2 = state.currentVideoInfo) === null || _state$currentVideoIn2 === void 0 ? void 0 : _state$currentVideoIn2.videoQuality) || {};
  const playBackSpeed = [{
    label: '0.25',
    value: 0.25
  }, {
    label: '0.5',
    value: 0.5
  }, {
    label: '0.75',
    value: 0.75
  }, {
    label: 'Normal',
    value: 1
  }, {
    label: '1.25',
    value: 1.25
  }, {
    label: '1.5',
    value: 1.5
  }, {
    label: '1.75',
    value: 1.75
  }, {
    label: '2',
    value: 2
  }];
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: _Styles.styles.selectQuality
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: _Styles.styles.width1_4
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: _Styles.styles.selectTextHeader
  }, "Qualities"), /*#__PURE__*/_react.default.createElement(_reactNative.ScrollView, null, videoQualities.map(element => {
    return /*#__PURE__*/_react.default.createElement(_reactNative.TouchableOpacity, {
      onPress: () => {
        dispatch({
          type: 'SET_VIDEO_QUALITY',
          videoQuality: element
        });
      },
      key: `index${element.label}`,
      style: _Styles.styles.qualityDetail
    }, element.height === videoQuality.height ? /*#__PURE__*/_react.default.createElement(_FontAwesome.default, {
      style: _Styles.styles.backward,
      name: 'check-circle-o',
      size: 14
    }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: 22.5
      }
    }), /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
      style: {
        fontSize: 12,
        color: '#FFF'
      }
    }, element.label));
  }))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: _Styles.styles.width1_4
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: _Styles.styles.selectTextHeader
  }, "Speed"), /*#__PURE__*/_react.default.createElement(_reactNative.ScrollView, null, playBackSpeed.map(element => {
    return /*#__PURE__*/_react.default.createElement(_reactNative.TouchableOpacity, {
      onPress: () => {
        dispatch({
          type: 'SET_PLAYBACK_SPEED',
          playbackSpeed: element.value
        });
      },
      key: `index${element.value}`,
      style: _Styles.styles.qualityDetail
    }, element.value === state.playbackSpeed ? /*#__PURE__*/_react.default.createElement(_FontAwesome.default, {
      style: _Styles.styles.backward,
      name: 'check-circle-o',
      size: 14
    }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: 22.5
      }
    }), /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
      style: {
        fontSize: 12,
        color: '#FFF'
      }
    }, element.label));
  }))));
};

var _default = SelectView;
exports.default = _default;
//# sourceMappingURL=SelectView.js.map