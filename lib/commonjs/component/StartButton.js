"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.StartButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactNative = require("react-native");

var _Styles = require("../Styles");

var _MaterialIcons = _interopRequireDefault(require("react-native-vector-icons/MaterialIcons"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const StartButton = _ref => {
  let {
    onStartPress
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_reactNative.TouchableOpacity, {
    style: [_Styles.styles.playButton],
    onPress: () => onStartPress()
  }, /*#__PURE__*/_react.default.createElement(_MaterialIcons.default, {
    style: [_Styles.styles.playArrow],
    name: "play-arrow",
    size: 42
  }));
};

exports.StartButton = StartButton;
var _default = StartButton;
exports.default = _default;
//# sourceMappingURL=StartButton.js.map