"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleBuildAdTagUrl = void 0;

/**
 * @description the function build adTagUrl based on stream config
 * @param state
 * @param conf
 * @param duration
 * @return {string}
 */
const handleBuildAdTagUrl = (state, conf, duration) => {
  var _state$mainAdUnit;

  const delay = parseInt(state.mainAdUnit.delay ? state.mainAdUnit.delay : conf.delay);
  const vmapgen = {
    delay: delay,
    interval: conf.interval,
    maxslots: conf.maxslots,
    mintimeleft: conf.mintimeleft,
    duration: duration ? duration : state.duration
  };

  const _vmapgen = JSON.stringify(vmapgen);

  let _vmapgenEncode = encodeURI(_vmapgen);

  const baseUrl = 'https://content.jixie.io/v1/video?vmapgen=';
  const source = '&source=jxplayer';
  const clientId = `&client_id=${state === null || state === void 0 ? void 0 : state.clientId}`;
  const sid = `&sid=${state === null || state === void 0 ? void 0 : state.sid}`;
  const pageurl = '&pageurl=https%3A%2F%2Fmegapolitan.kompas.com%2Fread%2F2021%2F05%2F28%2F05334261%2Fupdate-27-mei-bertambah-15-kasus-covid-19-di-tangsel-kini-totalnya-11257';
  const domain = '&domain=megapolitan.kompas.com';
  const unit = state !== null && state !== void 0 && (_state$mainAdUnit = state.mainAdUnit) !== null && _state$mainAdUnit !== void 0 && _state$mainAdUnit.id ? `&unit=${state.mainAdUnit.id}` : '';
  return baseUrl + _vmapgenEncode + source + clientId + sid + pageurl + domain + unit;
};

exports.handleBuildAdTagUrl = handleBuildAdTagUrl;
//# sourceMappingURL=handleBuildAdTagUrl.js.map